# AIS Digital
Repositório do teste relativo ao processo seletivo para a empresa AIS Digital

## Tecnologias

### Java 11
### Spring Boot
### RESTfull
### Maven
### Lombok
### Swagger
### Teste unitário com Junit/Mockito

------------------------------------------------

## Comandos para rodar a API
```
mvn clean install
mvn spring-boot:run
```

