package com.ais.desafio.service;

import com.ais.desafio.exception.RestTemplateResponseErrorHandler;
import com.ais.desafio.vo.TheMovieDBVO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TheMovieDBServiceTest {

    @Autowired
    private TheMovieDBService theMovieDBService;

    @Mock
    private RestTemplateResponseErrorHandler restTemplateResponseErrorHandler;

    @Test
    public void shouldToReturnCodeHttpOk() throws IOException {
        Mockito.when(restTemplateResponseErrorHandler.hasError(Mockito.any())).thenReturn(true);
        ResponseEntity<TheMovieDBVO> response = theMovieDBService.getMovie();
        Assert.assertEquals(200, response.getStatusCodeValue());
    }
}
