package com.ais.desafio.service;

import com.ais.desafio.exception.RestTemplateResponseErrorHandler;
import com.ais.desafio.vo.TheMovieDBVO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.List;

@Service
public class TheMovieDBService {

    private static final String URL_TMDB = "https://api.themoviedb.org/3/movie/550?api_key=c641994f64b9b61b88c664884dfda15b";

    private RestTemplate restTemplate;

    @Autowired
    public TheMovieDBService(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.errorHandler(new RestTemplateResponseErrorHandler()).build();
    }

    public ResponseEntity<TheMovieDBVO> getMovie() {
        return restTemplate.exchange(URL_TMDB, HttpMethod.GET, null,
                new ParameterizedTypeReference<>() {});
    }

}
