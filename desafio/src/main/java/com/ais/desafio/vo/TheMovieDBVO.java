package com.ais.desafio.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TheMovieDBVO {

   @JsonProperty(value = "id")
   private Integer id;

   @JsonProperty(value = "original_title")
   private String originalTitle;

   @JsonProperty(value = "homepage")
   private String homePage;

   @JsonProperty(value = "original_language")
   private String originalLanguage;

   @JsonProperty(value = "overview")
   private String overview;

}
