package com.ais.desafio.controller;

import com.ais.desafio.service.TheMovieDBService;
import com.ais.desafio.vo.TheMovieDBVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "Serviços responsáveis pelo consumo da API The Movie DB.", tags={ "TheMovieDBController"})
@RestController
@RequestMapping(value = "/rest/v1/themoviedb/", produces = "application/json")
public class TheMovieDBController {

    private final TheMovieDBService theMovieDBService;

    @Autowired
    public TheMovieDBController(TheMovieDBService theMovieDBService) {
        this.theMovieDBService = theMovieDBService;
    }

    @ApiOperation(value="Serviço responsável por recuperar um filme da API The Movie DB.")
    @GetMapping()
    public ResponseEntity<TheMovieDBVO> fetchMovie() {
        return ResponseEntity.status(HttpStatus.OK).body(theMovieDBService.getMovie().getBody());
    }

}